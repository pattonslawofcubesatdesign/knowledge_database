# CubeSat Mission
## Contents
- Mission Overview
- Ground Segment
  - Ground Station
  - Mission Control
- Space Segment
  - COM
  - OBC
  - EPS
  - ADCS
- General Aspects
  - Coding, Versioning, Reviewing
  - Testing
  - Interface Definitions
  - Trade Offs and Robustness
  - How to spot something is amiss

- Literature Collection